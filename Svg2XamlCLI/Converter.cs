﻿using Svg2Xaml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Markup;
using System.Xml;

namespace Svg2XamlCLI
{
    //
    public class Converter
    {
        private List<string> _inputs;
        private string _output;

        /// <summary>
        /// 出力ファイルより新しい日付の入力ファイルがある場合に限り実行する
        /// </summary>
        public bool IsUpdate { get; set; }

        /// <summary>
        /// コンストラクター
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public Converter(List<string> input, string output)
        {
            _inputs = input.Select(e => GetValidatePath(e)).SelectMany(e => e).ToList();
            _output = output ?? "Images.xaml";
        }

        /// <summary>
        /// ワイルドカードを展開して入力パスを取得
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private List<string> GetValidatePath(string path)
        {
            if (File.Exists(path))
            {
                return new List<string>() { path };
            }
            else if (Directory.Exists(path))
            {
                return Directory.GetFiles(path, "*.svg").ToList();
            }
            else
            {
                var directory = Path.GetDirectoryName(path);
                var pattern = Path.GetFileName(path);
                return Directory.GetFiles(directory, pattern).ToList();
            }
        }

        /// <summary>
        /// ResourceDictionary.Xaml生成
        /// </summary>
        public void Convert()
        {
            // 更新チェック
            if (this.IsUpdate && File.Exists(_output))
            {
                var outputTimestamp = File.GetLastWriteTime(_output);
                var inputTimestamp = _inputs.Select(e => File.GetLastWriteTime(e)).Max();

                if (inputTimestamp < outputTimestamp)
                {
                    Console.WriteLine("更新されませんでした");
                    return;
                }
            }

            // ResourceDictionary作成
            var resourceDictionary = new ResourceDictionary();
            foreach (var input in _inputs)
            {
                Console.WriteLine($"+ {input}");
                using (FileStream stream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    var drawingImage = SvgReader.Load(stream);
                    drawingImage.Freeze();
                    resourceDictionary.Add(Path.GetFileNameWithoutExtension(input), drawingImage);
                }
            }

            // Xaml出力
            using (var writer = XmlWriter.Create(_output, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true }))
            {
                XamlWriter.Save(resourceDictionary, writer);
            }

            Console.WriteLine($"\n出力: {_output}\n");
        }
    }

}
