﻿using Svg2Xaml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;

namespace Svg2XamlView
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        //
        private DrawingImage _DrawingImage;
        public DrawingImage DrawingImage
        {
            get { return _DrawingImage; }
            set { _DrawingImage = value; OnPropertyChanged(); }
        }

        //
        private string _KeyName;
        public string KeyName
        {
            get { return _KeyName; }
            set { _KeyName = value; OnPropertyChanged(); }
        }

        public string InputFileName { get; set; }


        //
        private string _StatusMessage;
        public string StatusMessage
        {
            get { return _StatusMessage; }
            set { _StatusMessage = value; OnPropertyChanged(); }
        }


        //
        public Stretch[] StretchList { get; private set; } = (Stretch[])Enum.GetValues(typeof(Stretch));

        /// <summary>
        /// SelectedStretch property.
        /// </summary>
        private Stretch _SelectedStretch;
        public Stretch SelectedStretch
        {
            get { return _SelectedStretch; }
            set { if (_SelectedStretch != value) { _SelectedStretch = value; OnPropertyChanged(); } }
        }


        /// <summary>
        /// BackgroundColor property.
        /// </summary>
        private Color _BackgroundColor = Colors.Black;
        public Color BackgroundColor
        {
            get { return _BackgroundColor; }
            set
            {
                if (_BackgroundColor != value)
                {
                    _BackgroundColor = value;
                    OnPropertyChanged(); OnPropertyChanged(nameof(BackgroundBrush));
                }
            }
        }

        public SolidColorBrush BackgroundBrush => new SolidColorBrush(_BackgroundColor);


        /// <summary>
        /// IsColorEnabled property.
        /// </summary>
        private bool _IsColorEnabled;
        public bool IsColorEnabled
        {
            get { return _IsColorEnabled; }
            set { if (_IsColorEnabled != value) { _IsColorEnabled = value; OnPropertyChanged(); } }
        }


        /// <summary>
        /// 
        /// </summary>
        public MainWindowViewModel()
        {
        }
    
        //
        public void Load(string fileName)
        {
            this.InputFileName = Path.GetFullPath(fileName);

            try
            {
                using (FileStream stream = new FileStream(this.InputFileName, FileMode.Open, FileAccess.Read))
                {
                    this.DrawingImage = SvgReader.Load(stream);
                    this.DrawingImage.Freeze();
                    this.KeyName = Path.GetFileNameWithoutExtension(this.InputFileName);
                }
                StatusMessage = $"読み込み完了: {System.IO.Path.GetFileName(this.InputFileName)}";
            }
            catch
            {
                DrawingImage = null;
                KeyName = null;
                StatusMessage = $"読み込みエラー: {System.IO.Path.GetFileName(this.InputFileName)}";
            }
        }

        public void Save(string fileName)
        {
            fileName = fileName ?? this.KeyName + ".xaml";

            try
            {
                var resourceDictionary = new ResourceDictionary();
                resourceDictionary.Add(this.KeyName, this.DrawingImage);
                using (var writer = XmlWriter.Create(fileName, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true }))
                {
                    XamlWriter.Save(resourceDictionary, writer);
                }

                StatusMessage = $"XAML 出力完了: {KeyName}";
            }
            catch
            {
                StatusMessage = "XAML 出力エラー";
            }
        }
    }
}
