# Svg2Xaml

fork of https://svg2xaml.codeplex.com

## History

* [オリジナル](https://svg2xaml.codeplex.com) をインポート
* [Svg2Xaml Stroke対応版](https://tgws.plus/dl/svg2xaml_s/) 適用
* Svg2XamlView,Svg2XamlCLI追加

## License

このコードはLGPLで配布されています。
詳細は Svg2Xaml\COPYING を参照してください

## Svg2XamlView

単体のSVGビューアー兼XAMLコンバーターです。  
SVGをドラッグ&ドロップして使用します。
出力されるXAMLはResourceDictionary形式です。

## Svg2XamlCLI

コマンドラインでのSVG2XAML変換を行います。  
複数のSVG入力からResourceDictionaryを作成し、1つのXAMLに出力します。