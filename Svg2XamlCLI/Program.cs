﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Svg2XamlCLI
{
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        static int Main(string[] args)
        {
            // コマンドライン引数を解析する
            var options = new Options();
            try
            {
                options.Parse(args);
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}\n");
                options.DumpHelp();
                return - 1;
            }


            // 変換
            try
            {
                var converter = new Converter(options.Inputs, options.Output) { IsUpdate = options.IsUpdate };
                converter.Convert();
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return -1;
            }
        }
    }

}
