﻿using System;
using System.Collections.Generic;

namespace Svg2XamlCLI
{
    /// <summary>
    /// 
    /// </summary>
    public class Options
    {
        public List<string> Inputs { get; set; } = new List<string>();
        public string Output { get; set; } = "Images.xaml";
        public bool IsUpdate { get; set; }

        //
        public void DumpHelp()
        {
            Console.WriteLine("Usage: Svg2XamlCLI.exe[options] files.svg...");
            Console.WriteLine("");
            Console.WriteLine("Options:");
            Console.WriteLine("    -o <filename> 出力ファイル名(省略時はImages.xaml)");
            Console.WriteLine("    -u                   出力ファイルより入力ファイルの日付が新しければコンバートを行う");
            Console.WriteLine("");
        }

        //
        public void Parse(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].StartsWith("-"))
                {
                    switch (args[i])
                    {
                        case "-o":
                            i++;
                            this.Output = args[i];
                            break;
                        case "-u":
                            this.IsUpdate = true;
                            break;
                        default:
                            throw new ArgumentException("サポートされていないオプションです");
                    }
                }
                else
                {
                    this.Inputs.Add(args[i]);
                }
            }
        }
    }

}
