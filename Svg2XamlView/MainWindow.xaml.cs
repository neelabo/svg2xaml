﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Svg2XamlView
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowViewModel _vm;

        public MainWindow()
        {
            InitializeComponent();

            _vm = new MainWindowViewModel();
            this.DataContext = _vm;
        }


        private void ContentArea_Drop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files != null)
            {
                _vm.Load(files[0]);
            }
        }

        private void ContentArea_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void Convert_Button(object sender, RoutedEventArgs e)
        {
            if (_vm.DrawingImage == null) return;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(_vm.InputFileName);
            saveFileDialog.DefaultExt = ".xaml";
            saveFileDialog.FileName = System.IO.Path.GetFileNameWithoutExtension(_vm.InputFileName) + ".xaml";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.Filter = "XAML ファイル(.xaml)|*.xaml";
            bool? result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                _vm.Save(saveFileDialog.FileName);
            }
        }

        private void ColorSample_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var dialog = new System.Windows.Forms.ColorDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _vm.BackgroundColor = Color.FromArgb(dialog.Color.A, dialog.Color.R, dialog.Color.G, dialog.Color.B);
            }
        }
    }



    // コンバータ：
    [ValueConversion(typeof(bool), typeof(Stretch))]
    public class BooleanIsFillToStretchConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isFill = false;
            if (value is bool)
            {
                isFill = (bool)value;
            }

            return isFill ? Stretch.Fill : Stretch.Uniform;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
